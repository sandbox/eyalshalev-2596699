<?php

/**
 * @file
 * Contains Drupal\base_entity\Entity\BaseContentEntityViewsData.
 */

namespace Drupal\base_entity\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Base content entity entities.
 */
class BaseContentEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['base_content_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Base content entity'),
      'help' => $this->t('The Base content entity ID.'),
    );

    return $data;
  }

}
