<?php

/**
 * @file
 * Contains Drupal\base_entity\BaseContentEntityListBuilder.
 */

namespace Drupal\base_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Base content entity entities.
 *
 * @ingroup base_entity
 */
class BaseContentEntityListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Base content entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\base_entity\Entity\BaseContentEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $this->getLabel($entity),
      new Url(
        'entity.base_content_entity.edit_form', array(
          'base_content_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
