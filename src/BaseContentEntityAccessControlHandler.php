<?php

/**
 * @file
 * Contains Drupal\base_entity\BaseContentEntityAccessControlHandler.
 */

namespace Drupal\base_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Base content entity entity.
 *
 * @see \Drupal\base_entity\Entity\BaseContentEntity.
 */
class BaseContentEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view base content entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit base content entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete base content entity entities');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add base content entity entities');
  }

}
