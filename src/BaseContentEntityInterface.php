<?php

/**
 * @file
 * Contains Drupal\base_entity\BaseContentEntityInterface.
 */

namespace Drupal\base_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Base content entity entities.
 *
 * @ingroup base_entity
 */
interface BaseContentEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

}
